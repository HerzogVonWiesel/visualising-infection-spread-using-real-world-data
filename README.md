# Simulating and visualising infection spread using real-world movement and contact data

This project hosts the centerpiece of my Bachelors Thesis, which is the Houdini file in which the model lives and thrives in.

There are some more additional base files to keep track off, for example the GeoJSON for the german districts.
